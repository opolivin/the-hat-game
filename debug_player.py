from pathlib import Path
from glob import glob

from nltk.stem.snowball import SnowballStemmer

import numpy as np
import pandas as pd
import fasttext
from nltk.metrics.distance import edit_distance
from sklearn.metrics.pairwise import cosine_similarity
from tqdm import tqdm

# from data.utils import merge_all_files
from the_hat_game.game import Game
from the_hat_game.players import PlayerDefinition, AbstractPlayer, RemotePlayer

pd.set_option('display.max_colwidth', 200)

stemmer = SnowballStemmer("english")



class LocalFasttextPlayer(AbstractPlayer):
    def __init__(self, model):
        self.model = model
        self.to_exclude = set()
        self.words = model.get_words()
        self.matrix = np.concatenate([model[word].reshape(1, -1) for word in self.words], axis=0)
        self.stemmer = SnowballStemmer("english")

    def stem_and_set(self, words):
        stemmer = self.stemmer
        stemmed_set = set()
        if isinstance(words, list):
            for word in words:
                stemmed_set.add(stemmer.stem(word))

        if isinstance(words, str):
            for word in words.split():
                stemmed_set.add(stemmer.stem(word))

        return stemmed_set


    def is_redundant(self, bag_of_words, word_to_check):
        for word in bag_of_words:

            if word_to_check == word:
                return True

            # if word in word_to_check:
            #     return True

            if edit_distance(word, word_to_check) <= 2:
                return True
        return False


    def find_words_for_sentence(self, sentence, n_closest):
        stemmer = self.stemmer
        to_exclude = self.to_exclude
        # print('This is to exclude set: ', len(to_exclude))
        neighbours = self.model.get_nearest_neighbors(sentence, 500)
        # words = [word for similariry, word in neighbours][:n_closest]
        stem_and_set = self.stem_and_set(sentence)
        # print('StemAndSet', stem_and_set)
        to_exclude.update(stem_and_set)
        words = []
        # print('Neigh', len(neighbours))
        for similarity, neighbour in neighbours:
            # print('Words', len(words), words)
            stemmed_neighbour = stemmer.stem(neighbour)
            
            if self.is_redundant(to_exclude, stemmed_neighbour):
                to_exclude.add(stemmed_neighbour)
                # print(len(to_exclude), stemmed_neighbour)
                continue
            words.append(neighbour)
            # print(len(to_exclude), stemmed_neighbour)
            to_exclude.add(stemmed_neighbour)

            if len(words) == n_closest:
                break
        self.to_exclude.update(to_exclude)
        return words


    def find_words_for_vector(self, vector, n_closest):
        sims = cosine_similarity(vector.reshape(1, -1), self.matrix).ravel()
        word_sims = pd.Series(sims, index=self.model.get_words()).sort_values(ascending=False)
        return list(word_sims.head(n_closest).index)

    def find_words_for_list_of_words(self, sentence, n_words):
        stemmer = self.stemmer
        to_exclude = self.to_exclude

        stem_and_set = self.stem_and_set(sentence)
        to_exclude.update(stem_and_set)

        vector = self.model.get_sentence_vector(sentence)
        neighbours = self.find_words_for_vector(vector, n_closest=500)
        words = []
        for neighbour in neighbours:
            # print('Words', len(words), words)
            stemmed_neighbour = stemmer.stem(neighbour)
            # print(stemmed_neighbour, self.is_redundant(to_exclude, stemmed_neighbour))
            if self.is_redundant(to_exclude, stemmed_neighbour):
                to_exclude.add(stemmed_neighbour)
                # print(len(to_exclude), stemmed_neighbour)
                continue
            words.append(neighbour)
            # print(len(to_exclude), stemmed_neighbour)
            to_exclude.add(stemmed_neighbour)

            if len(words) == n_words:
                break
        self.to_exclude.update(to_exclude)
        return words

    def explain(self, word, n_words):
        self.to_exclude = set()
        return self.find_words_for_sentence(word, n_words)

    def guess(self, words, n_words):
        if len(words) == 1:
            self.to_exclude = set()
        return self.find_words_for_list_of_words(' '.join(words), n_words)



player = LocalFasttextPlayer(model=fasttext.load_model("models/2021-06-23.model"))
# print(player.explain('forbidden', 10))
# # ['ambition', 'broadly', 'coalition', 'profound', 'deprived', 'covetous', 'militiamen', 'influenza', 'antidepressants', 'abroad']

# print('Player explain word: work')

words = ['abuja']
# to_guess = ' '.join(words).strip()
# print(to_guess)
print(player.guess(words, 10))

words = ['to']
print(player.guess(words, 10))

words = ['to', 'i']
print(player.guess(words, 10))

words = ['to', 'i', 'agree']
print(player.guess(words, 10))

words = ['to', 'i', 'agree', 'o']
print(player.guess(words, 10))

words = ['to', 'i', 'agree', 'o', 'yer']
print(player.guess(words, 10))


# words = ['to', 'i','agree', 'o', 'yer', 'that', 'vandalll']
# print(player.guess(words, 20))
# # print(player.to_exclude)
# # words = ['abuja', 'nigeria', 'africa']
# # to_guess = ' '.join(words).strip()
# # print(to_guess)
# # print(player.guess(words, 10))

# print(player.explain('nigeria', 10))
# replace
# print("Guess word 'replace'")
# print(player.guess(['refreshing', 'restarting', 'looking', 'sell', 'a', 'sticky'], 15))

# print('Guess word "a"')
# print(player.guess(['a'], 15))
