from glob import glob
import csv
import nltk
from bs4 import BeautifulSoup
import re
from tqdm import tqdm

from nltk.corpus import stopwords
from nltk.tokenize import RegexpTokenizer
from pathlib import Path
from typing import List

from nltk.stem import WordNetLemmatizer

nltk.download("stopwords")

tokenizer = RegexpTokenizer(r'\w+')
stopword_set = set(stopwords.words('english'))
lemmatizer_nltk = WordNetLemmatizer()



def open_20newsgroups(filepath:str):
    data = []
    with open(filepath) as infile:
        for line in infile.readlines():
            if line.startswith((
                'Xref',
                'Path',
                'From',
                'Newsgroups',
                'Subject',
                'Followup-To',
                'Reply-To',
                'References',
                'Distribution',
                'Lines',
                'In article',
                '>',
                'Date',
                'Organization',
                'Lines',
                'Sender',
                'Message-ID',
                'NNTP',
                'To'
                )):
                continue
            line = remove_urls(line)
            data.append(line.strip())
        data = [line for line in data if line.strip()]
    return ' '.join(data)

def open_blobs(filepath:str):
    data = []
    with open(filepath) as infile:
        reader = csv.reader(infile)
        next(reader)
        for i, row in enumerate(reader):
            if len(row) == 1:
                yield remove_urls(row[0]).strip()
            else:
                yield remove_urls(row[1]).strip()

def tokenize_data(text: str):
    return tokenizer.tokenize(text)

def remove_short(tokens: List):
    return [token for token in tokens if len(token) > 3]

def remove_urls(text:str):
    text = re.sub(r'http\S+', '', text)
    return text

def keep_onlyalpha(tokens: List):
    onlyalphalist = [token for token in tokens if token.isalpha()]
    return onlyalphalist

def remove_stopwords(tokens: List):
    nostopwords = [token for token in tokens if token not in stopword_set]
    return nostopwords

def lemmatize(doc):
    tokens = []
    for token in doc:
        tokens.append(token.lemma_.lower())
    return tokens

def remove_html_tags(text: str):
    soup = BeautifulSoup(text, "html.parser")
    for elm in soup.select('a'):
        elm.extract()
    return str(soup)


def lemmatize_nltk(tokens: List):

    lemmatized = [lemmatizer_nltk.lemmatize(token) for token in tokens]
    return lemmatized

if __name__ == '__main__':
    file_path =  Path('/training/data/preprocessed.txt')
    folder = Path('/training/data/20_newsgroups/')

    with open(file_path, "w") as write_stream:
        files = glob(str(folder / "**" / "*"))

        for path in tqdm(files[:]):
                try:
                    text = open_20newsgroups(path)
                    text = remove_html_tags(text.strip())
                    if not text:
                        continue
                    tokenized_list = tokenize_data(text.strip())
                    # keep_long = remove_short(tokenized_list)
                    alpha_tokens = keep_onlyalpha(tokenized_list)
                    # nostopwords = remove_stopwords(alpha_tokens)
                    processed_txt = ' '.join(alpha_tokens).lower()
                    print(processed_txt.encode('utf-8').decode('utf-8'), file=write_stream)
                except (UnicodeEncodeError, UnicodeDecodeError):
                    pass

    file_path =  Path('/training/data/preprocessed.txt')
    folder = Path('/training/data/blobs/')
    with open(file_path, "a") as write_stream:
        files = glob(str(folder / "*"))
        for path in tqdm(files[:]):
            try:
                rows = open_blobs(path)
                for row in rows:
                    if not row:
                        continue
                    row = remove_html_tags(row.strip())
                    if not row:
                        continue
                    tokenized_list = tokenize_data(row.strip())
                    # keep_long = remove_short(tokenized_list)
                    alpha_tokens = keep_onlyalpha(tokenized_list)
                    # nostopwords = remove_stopwords(alpha_tokens)
                    processed_txt = ' '.join(alpha_tokens).lower()
                    print(processed_txt.encode('utf-8').decode('utf-8'), file=write_stream)
            except (UnicodeEncodeError, UnicodeDecodeError):
                pass
 
