from pathlib import Path
from glob import glob
from tqdm import tqdm


file_path =  Path('/training/data/20-newsgroups.txt')
folder = Path('/training/data/20_newsgroups/')

with open(file_path, "w") as write_stream:
    files = glob(str(folder / "**" / "*"))

    for path in tqdm(files[:]):
        with open(path, encoding="utf-8") as stream:
            try:
                text = stream.read()
                print(text.encode('utf-8').decode('utf-8'), file=write_stream)
            except (UnicodeEncodeError, UnicodeDecodeError):
                pass
