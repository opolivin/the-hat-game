from pathlib import Path
from glob import glob

import numpy as np
import pandas as pd
import fasttext
from sklearn.metrics.pairwise import cosine_similarity
from tqdm import tqdm

file_path = Path('/training/data/preprocessed.txt')
model_skipgram = fasttext.train_unsupervised(str(file_path), model='skipgram', dim=200, maxn=4, epoch=100)
model_skipgram.save_model('/training/models/gke.model')

