#!/bin/sh
# echo "Reading data to file"
# python scripts/read_data_to_file.py
echo "Downloading bucket data"
python scripts/download_bucket_data.py
echo "Preprocess the data"
python scripts/preprocessing.py
echo "Train the model"
python scripts/train.py
# echo "Running ML training pipeline"
# python scripts/train.py
# echo "Listing the resulting files and folders"
# ls -R .
