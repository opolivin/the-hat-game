from google.cloud import storage

import pathlib

def download_public_file(bucket_name, destination_dir):
    """Downloads a public blob from the bucket."""
    # comment just to relaunch pipeline
    # bucket_name = "your-bucket-name"
    # source_blob_name = "storage-object-name"
    # destination_file_name = "local/path/to/file"

    storage_client = storage.Client.create_anonymous_client()

    bucket = storage_client.bucket(bucket_name)
    for blob in bucket.list_blobs(prefix=''):
        destination_file_name = destination_dir + blob.name
        blob.download_to_filename(destination_file_name)



if __name__ == '__main__':
    dst_dir = '/training/data/blobs/'
    pathlib.Path(dst_dir).mkdir(parents=True, exist_ok=True) 
    dmia_data = 'dmia-mlops-texts'
    download_public_file(dmia_data, dst_dir)
