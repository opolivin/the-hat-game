from pathlib import Path
from glob import glob

import numpy as np
import pandas as pd
import fasttext
from sklearn.metrics.pairwise import cosine_similarity
from tqdm import tqdm

file_path = Path('texts/preprocessed.txt')
model_skipgram = fasttext.train_unsupervised(str(file_path), model='skipgram', dim=150, maxn=4, epoch=40)
model_skipgram.save_model('models/2021-06-23.model')

