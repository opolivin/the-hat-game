import fasttext
import numpy as np
import pandas as pd

from settings import CRITERIA, N_EXPLAIN_WORDS, N_GUESSING_WORDS  # , N_ROUNDS
from the_hat_game.game import Game

from nltk.metrics.distance import edit_distance
from sklearn.metrics.pairwise import cosine_similarity
# from the_hat_game.google import get_players
from the_hat_game.players import PlayerDefinition, AbstractPlayer, RemotePlayer
from nltk.stem.snowball import SnowballStemmer
stemmer = SnowballStemmer("english")

class LocalFasttextPlayer(AbstractPlayer):
    def __init__(self, model):
        self.model = model
        self.to_exclude = set()


    def stem_and_set(self, words):
        stemmed_set = set()
        if isinstance(words, list):
            for word in words:
                stemmed_set.add(stemmer.stem(word))

        if isinstance(words, str):
            for word in words.split():
                stemmed_set.add(stemmer.stem(word))

        return stemmed_set


    def is_redundant(self, bag_of_words, word_to_check):
        for word in bag_of_words:

            if word_to_check in word:
                return True

            if word in word_to_check:
                return True

            if edit_distance(word, word_to_check) <= 2:
                return True
        return False


    def find_words_for_sentence(self, sentence, n_closest, words=None):
        to_exclude = self.to_exclude

        neighbours = self.model.get_nearest_neighbors(sentence, 250)
        # words = [word for similariry, word in neighbours][:n_closest]
        stem_and_set = self.stem_and_set(sentence)
        to_exclude.update(stem_and_set)
        words = []
        # print('Neigh', len(neighbours))
        for similarity, neighbour in neighbours:
            # print('Words', len(words), words)
            stemmed_neighbour = stemmer.stem(neighbour)

            if self.is_redundant(to_exclude, stemmed_neighbour):
                to_exclude.add(stemmed_neighbour)
                continue
            words.append(neighbour)
            to_exclude.add(stemmed_neighbour)

            if len(words) == n_closest:
                break

        return words

    def explain(self, word, n_words):
        self.to_exclude = set()
        return self.find_words_for_sentence(word, n_words)

    def guess(self, words, n_words):
        if len(words) == 1:
            self.to_exclude = set()
        return self.find_words_for_sentence(' '.join(words), n_words, words)

player = LocalFasttextPlayer(model=fasttext.load_model("models/2021_06_14_processed.model"))

if __name__ == "__main__":

    # data = get_players()

    PLAYERS = [
        PlayerDefinition('HerokuOrg team', RemotePlayer('https://obscure-everglades-02893.herokuapp.com')),
        PlayerDefinition('Make Hat Game Again', RemotePlayer('http://35.246.139.13/')),
        PlayerDefinition('Very Local Player', player)
    ]

    while True:
        N_WORDS = 8
        WORDS = []
        for vocabulary_path in [
            #     'text_samples/verbs_top_50.txt',
            "text_samples/nouns_top_50.txt",
            #     'text_samples/adjectives_top_50.txt',
        ]:
            print(vocabulary_path)
            with open(vocabulary_path) as f:
                words = f.readlines()
                np.random.shuffle(words)
                words = [word.strip() for word in words][:N_WORDS]
                WORDS.extend(words)

        game = Game(
            PLAYERS,
            WORDS,
            CRITERIA,
            len(WORDS),
            N_EXPLAIN_WORDS,
            N_GUESSING_WORDS,
            random_state=0,
        )

        game_start = pd.Timestamp.now()
        game.run(verbose=True, complete=False)
        game_end = pd.Timestamp.now()
        # display(game_end - game_start)
